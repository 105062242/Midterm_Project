var LogOut = document.getElementById("LogOut");
var Username = document.getElementById("Username");
var Nickname = document.getElementById("Nickname");
var LoginCount = document.getElementById("Login-count");
var ValidArticle = document.getElementById("Valid-article");
var FinancialSituation = document.getElementById("Financial-situation");
var UserEmail = document.getElementById("User-email");
var LastTimeLogin = document.getElementById("Last-time-Login");
var user_email;
var user_name;
var profile_ID;
var set_profile = document.querySelector('set_profile');
var settingBtn = document.getElementById("settingBtn");
var done_setting = document.getElementById("done-setting");
var ProfileImage = document.getElementById('profile-image');
var setImage = document.getElementById("set-image");
var new_Username;
var new_Nickname;
var current_Username;
var current_Nickname;
//var Ref= firebase.database().ref("User_list");
var profile_pic;
function PreviewImage()
{
    var oFReader = new FileReader();
    var file = profile_pic.files[0];
    oFReader.readAsDataURL(file);
    oFReader.onload = function (oFREvent) {
        ProfileImage.src = oFREvent.target.result;
    };

    const metadata = {
        contentType: file.type
    };

    firebase.storage().ref("User_list/" + profile_ID + "/user_image").put(file,metadata).then((snapshot) => {
        const url = snapshot.downloadURL;
        firebase.database().ref("User_list/" + profile_ID + "/imageURL").set(url);
        //console.log(url);
        
    })
    //firebase.database().ref();

    /*
    alert(profile_pic.files[0]);
    ProfileImage.src = profile_pic.files[0];*/
}
function Btn_set_up()
{
    settingBtn.style.display = 'inline';
    done_setting.style.display = 'none';
    setImage.style.display = 'none';

    settingBtn.addEventListener('click',function(){
        done_setting.style.display = 'inline';
        settingBtn.style.display = 'none';
        setImage.style.display = 'block';     
        profile_pic = document.getElementById('profile_pic');
        
        Username.innerHTML = "<input type = 'text' value='" + current_Username + "' id='new-Username'>";
        Nickname.innerHTML = "<input type = 'text' value='" + current_Nickname + "' id='new-Nickname'>";
        new_Username = document.getElementById("new-Username");
        new_Nickname = document.getElementById("new-Nickname");
        var max_Username;
        new_Username.oninput = function()
        {
            if(new_Username.value.length <= 20)  max_Username = new_Username.value;
            else
            {
                new_Username.value = max_Username;
                alert("too long ! idiot !");
            }                                  
        };
        var max_Nickname;
        new_Nickname.oninput = function()
        {
            if(new_Nickname.value.length <= 20)  max_Nickname = new_Nickname.value;
            else
            {
                new_Nickname.value = max_Nickname;
                alert("too long ! idiot !");
            }                                  
        };
    });

    done_setting.addEventListener('click',function(){

        current_Username = new_Username.value;
        var tmp = new_Username.value;
        Username.innerHTML = tmp;
        firebase.database().ref("User_list/" + profile_ID + "/name").set(tmp);
        current_Username = tmp;

        current_Nickname = new_Nickname.value;
        tmp = (current_Nickname == "") ? "尚未設定" : current_Nickname;
        Nickname.innerHTML = tmp;
        firebase.database().ref("User_list/" + profile_ID + "/nickname").set(tmp);
        
        setImage.style.display = 'none';
        done_setting.style.display = 'none';
        settingBtn.style.display = 'inline';
        
    });
}
function init()
{
    settingBtn.style.display = 'none';
    done_setting.style.display = 'none';
    setImage.style.display = 'none';
    firebase.auth().onAuthStateChanged(function (user) {
        // Check user login
        if (user)
        {
            profile_ID = firebase.auth().currentUser.uid;
            /*
            var oFReader = new FileReader();
            firebase.storage().ref().child("User_list/" + profile_ID + "/user_image.jpg").then(function(){});
            //alert(file);
            alert(firebase.storage().ref().child("User_list/" + profile_ID + "/user_image").getDownloadURL());
            
            oFReader.readAsDataURL(file);
            oFReader.onload = function (oFREvent) {
                ProfileImage.src = oFREvent.target.result;
            };
*/
            user_email = user.email;
            user_name = user.displayName;
            var Ref = firebase.database().ref("User_list/" + profile_ID);
            var coin_str;
            var coin_count = 0;

            Ref.once('value').then(function(snapshot){
                var data = snapshot.val();
                firebase.database().ref('article_list').once('value').then(function(A_snapshot){
                    var A_data = A_snapshot.val();
                    if(data.article_list == null)
                    {

                    }
                    else
                    {
                        for(var i=0;i<data.article_list.length;++i)
                        {
                            //alert(A_data[data.article_list[i]].evaluation);
                            coin_count += A_data[data.article_list[i]].evaluation;
                        }
                    }

                }).then(function(){

                    if(data.imageURL != null)
                    {
                        //alert("d");
                        ProfileImage.src = data.imageURL;
                    }
                    data.financial = coin_count;
                    if(data.financial < -10)        coin_str = "(臭臭的乞丐)";
                    else if(data.financial < 0)     coin_str = "(乞丐)";
                    else if(data.financial < 10)    coin_str = "(窮光蛋)";
                    else if(data.financial < 50)    coin_str = "(還是窮光蛋)";
                    else if(data.financial < 100)   coin_str = "(已經買得起衣服的窮光蛋)";
                    else if(data.financial < 200)   coin_str = "(普通的平民)";
                    else if(data.financial < 500)   coin_str = "(稍微富有的平民)";
                    else if(data.financial < 1000)  coin_str = "(有錢人)";
                    else                            coin_str = "(臭有錢人)";
                    current_Username = data.name;
                    current_Nickname = (data.nickname != null && data.nickname != "尚未設定") ? data.nickname : "";

                    Username.textContent = data.name;
                    if(data.nickname == "" || data.nickname == null) Nickname.textContent = "尚未設定";
                    else                    Nickname.textContent = data.nickname;
                    LoginCount.textContent = data.LoginCount + "次";
                    ValidArticle.textContent = data.article + "篇文章";
                    FinancialSituation.textContent = data.financial + "個友善貨幣" + coin_str;
                    UserEmail.textContent = data.email;
                    LastTimeLogin.textContent = data.LastTimeLogin;

                    Btn_set_up();
                });

                
            });
            
            LogOut.addEventListener('click', e => {
                firebase.auth().signOut().then(function(e)
                {
                    alert("log out successfully!!");
                }).catch(function(e)
                {
                    alert("error",e.message);
                });
            });
        } else {
            window.location.href = "auth.html";
        }
    });
/*
    var storageRef = firebase.storage().ref();
    var mountainsRef = storageRef.child('mountains.jpg');
*/
/*
    setImage.style.display = 'none';

    settingBtn.addEventListener('click',function(){
        done_setting.style.display = 'inline';
        settingBtn.style.display = 'none';
        setImage.style.display = 'block';     
        profile_pic = document.getElementById('profile_pic');
        
        Username.innerHTML = "<input type = 'text' value='" + current_Username + "' id='new-Username'>";
        Nickname.innerHTML = "<input type = 'text' value='" + current_Nickname + "' id='new-Nickname'>";
        new_Username = document.getElementById("new-Username");
        new_Nickname = document.getElementById("new-Nickname");
    });
    
    done_setting.style.display = 'none';
    done_setting.addEventListener('click',function(){

        current_Username = new_Username.value;
        var tmp = new_Username.value;
        Username.innerHTML = tmp;
        firebase.database().ref("User_list/" + profile_ID + "/name").set(tmp);
        current_Username = tmp;

        current_Nickname = new_Nickname.value;
        tmp = (current_Nickname == "") ? "尚未設定" : current_Nickname;
        Nickname.innerHTML = tmp;
        firebase.database().ref("User_list/" + profile_ID + "/nickname").set(tmp);
        
        setImage.style.display = 'none';
        done_setting.style.display = 'none';
        settingBtn.style.display = 'inline';
        
    });

*/


}

window.onload = function()
{
    init();
}

/*
name            : user.displayName,
LoginCount      : 1,
article         : 0,
financial       : 0,
email           : email,
LastTimeLogin   : CurrentTime()
*/ 