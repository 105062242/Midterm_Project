function CurrentTime()
{
    //mins = ('0'+current_date.getMinutes()).slice(-2);
    var year = new Date().getFullYear();
    //var month = new Date().getMonth() + 1;
    var month = ('0' + (new Date().getMonth() + 1)).slice(-2);
    
    var day = ('0' + new Date().getDate()).slice(-2);
    
    var hour = ('0' + new Date().getHours()).slice(-2);
    
    var minute = ('0' + new Date().getMinutes()).slice(-2);
    
    return year + "/" + month + "/" + day + " " + hour + ":" + minute;
    //return year + "/" + month_zero + month + "/" + day_zero + day + " " + hour_zero + hour + ":" + minute_zero + minute;
}
function init()
{
    var txtEmail = document.getElementById('txtEmail');
    var txtPassword = document.getElementById('txtPassword');
    var txtUsername = document.getElementById("txtUsername");
    var btnLogin = document.getElementById('btnLogin');
    var btnSignUp = document.getElementById('btnSignUp');
    var btnLogout = document.getElementById('btnLogout');
    var btnGoogle = document.getElementById("btnGoogle");
    var fail_to_log_in = document.getElementById("fail_to_log_in");
    btnLogin.addEventListener('click', function(){
        var email = txtEmail.value;
        var password = txtPassword.value;
        if(email != "" && password != "")
        {
            txtPassword.value = "";
            firebase.auth().signInWithEmailAndPassword(email, password)
            .then(function()
            {
                var user = firebase.auth().currentUser;
                var Ref = firebase.database().ref("User_list/" + user.uid);
                Ref.once('value').then(function(snapshot){
                    var data = snapshot.val();
                    if(data != null)
                    {
                        firebase.database().ref("User_list/" + user.uid).set({
                            UID             : user.uid,
                            name            : data.name,
                            nickname        : data.nickname,
                            LoginCount      : data.LoginCount + 1,
                            article         : data.article,
                            financial       : data.financial,
                            email           : email,
                            LastTimeLogin   : CurrentTime(),
                            imageURL        : data.imageURL
                        }).then(function(){
                            window.location.href = 'index.html';
                        });
                    }
                    else
                    {
                        Ref.set({
                            UID             : user.uid,
                            name            : username,
                            nickname        : "",
                            LoginCount      : 1,
                            article         : 0,
                            financial       : 0,
                            email           : email,
                            LastTimeLogin   : CurrentTime()
                        }).then(function(){                          
                            window.location.href = 'index.html';
                        });
                    }
                    
                });
                

                
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                alert(errorMessage);
                if(errorCode === 'auth/wrong-password' ||errorCode === "auth/invalid-email" ||errorCode === "auth/user-not-found")
                {
                    fail_to_log_in.textContent = "wrong email or passwords !!";
                    fail_to_log_in.style.color = "red";
                }
                
                txtEmail.value = "";
                txtPassword.value = "";
                //console.log(error);
                
            });
        }
        else
        {
            alert("make sure you type everything , asshole.");
        }
        
        //alert("hi");

    });

    btnSignUp.addEventListener('click', function () {        
        var email = txtEmail.value;
        var password = txtPassword.value;
        var username = txtUsername.value;
        if(username != "")
        {
            txtPassword.value = "";
            txtUsername.value = "";
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(function()
            {
                //alert("hi");
                txtEmail.value = "";
                txtPassword.value = "";
                alert("You've successfully signed up!");
                var user = firebase.auth().currentUser;
                //var Ref = firebase.database().ref("User_list/" + user.uid);
                firebase.database().ref("User_list/" + user.uid).set({
                    UID             : user.uid,
                    name            : username,
                    nickname        : "",
                    LoginCount      : 1,
                    article         : 0,
                    financial       : 0,
                    email           : email,
                    LastTimeLogin   : CurrentTime()
                }).then(function(){

                    firebase.auth().currentUser.updateProfile({
                        displayName: username
                    }).then(function() {
                        window.location.href = 'index.html';
                    });

                });
                
            })
            .catch(function(error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                alert(errorMessage);
            });
        }
        else
        {
            alert("Please enter your Username");
        }
            
        
    });
/*
    btnLogout.addEventListener('click', function{
        firebase.auth().signOut();
    });
*/
    // Login with Google
    btnGoogle.addEventListener('click', function () {
        //alert("hi");
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result) {
            //alert(result.user);
            /*
            var token = result.credential.accessToken;
            var user = result.user;
            */
            var user = firebase.auth().currentUser;
            var Ref = firebase.database().ref("User_list/" + user.uid);
            Ref.once('value').then(function(snapshot){
                var data = snapshot.val();
                if(data != null)
                    {
                        firebase.database().ref("User_list/" + user.uid).set({
                            UID             : user.uid,
                            name            : data.name,
                            nickname        : data.nickname,
                            LoginCount      : data.LoginCount + 1,
                            article         : data.article,
                            financial       : data.financial,
                            email           : data.email,
                            LastTimeLogin   : CurrentTime(),
                            imageURL        : data.imageURL
                        }).then(function(){
                            window.location.href = 'index.html';
                        });
                    }
                    else
                    {
                        firebase.database().ref("User_list/" + user.uid).set({
                            UID             : user.uid,
                            name            : user.displayName,
                            nickname        : "",
                            LoginCount      : 1,
                            article         : 0,
                            financial       : 0,
                            email           : user.email,
                            LastTimeLogin   : CurrentTime()
                        }).then(function(){                          
                            window.location.href = 'index.html';
                        });
                    }

            });
            //alert(user.uid);
            //window.location.href = 'index.html';
        }).catch(function (error) {
            //console.log('error: ' + error.message);
            var errorMessage = error.message;
            //alert(errorMessage);
        });
       // alert("hi");
    });
    
    var username = document.getElementById('username');
    
    
}
window.onload = function()
{
    init();
};
/*
firebase.auth().onAuthStateChanged(user => {
    if (user) {
        window.location.href = 'index.html';
        //window.location.assign('https://midtermproject-105062242.firebaseapp.com/test.html');  //it can work perfectly too
    }
    else {
        console.log('not logged in');
        usrDiv.style.visibility = 'hidden';
    }
});*/
/*
firebase.auth().onAuthStateChanged(user => {
    if (user) {
        console.log(user);
        usrDiv.style.visibility = 'visible';
        usrName.innerHTML = 'User Name: ' + user.displayName;
        useEmail.innerHTML = 'User Email: ' + user.email;
    } else {
        console.log('not logged in');
        usrDiv.style.visibility = 'hidden';
    }
});
*/