function get_eval_str(number)
{
    //alert(number);
    if(number > 100)
    {
        return "<span class='eval_num' style='color:#f66;'>爆</span>";
    }
    else if(number > 0)
    {
        return "<span class='eval_num' style='color:#6f6;'>" + number + "</span>";
    }
    else if(number <= -100)
    {
        return "<span class='eval_num' style='color:#666;'>ＸＸ</span>";
    }
    else if(number <= -10)
    {
        return "<span class='eval_num' style='color:#666;'>Ｘ" + parseInt((-number)/10) + "</span>";
    }
    else
    {
        var str = "\xa0\xa0\xa0\xa0\xa0\xa0";
        return "<span class='eval_num'>" + str + "</span>";
    }
}
function init()
{
    //firebase.initializeApp({messagingSenderId: "933210089278"});
    //const messaging = firebase.messaging();
/*
    messaging.requestPermission().then(function() {
        console.log('Notification permission granted.');
        // TODO(developer): Retrieve an Instance ID token for use with FCM.
        // ...
    }).catch(function(err) {
        console.log('Unable to get permission to notify.', err);
    });
*/
    




    // var LogIn = document.getElementById("LogIn");
    var LogOut = document.getElementById("LogOut");
    var username = document.getElementById("username");
    
    firebase.auth().onAuthStateChanged(function (user){

        if(user)
        {
            user_email = user.email;
            // LogIn.style.display = "none";
            username.textContent =user.displayName;
            LogOut.style.display = "inline";
            LogOut.addEventListener('click', e => {
                firebase.auth().signOut().then(function(e)
                {
                    alert("log out successfully!!");
                }).catch(function(e)
                {
                    alert("error",e.message);
                });
            });
        }
        else
        {
            // LogIn.style.display = "inline";
            username.innerHTML = "";
            LogOut.style.display = "none";
            window.location.href = "auth.html";
        }

    });

    var a = location.href;
    var page;
    if(a.search(/[?]/g) == -1)
    {
        page = 1;
    }
    else
    {
        var b = a.substring(a.indexOf("?")+1);
        if(b == "") page = 1;
        else        page = Number(b);
    }
    
    var postsRef = firebase.database().ref('article_list');
    var Ref_len;
    var oldest_page;
    // List for store posts html
    var total_ArticleTitle = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    var post_article = document.getElementById("post_article");

    //var Mark;
    postsRef.once('value')
        .then(function (snapshot) {

            const APP = 10;//Article Per Page
            if(snapshot.val() == null)   Ref_len = 0;
            else    Ref_len = snapshot.val().length;
            oldest_page = Math.ceil(Ref_len/APP);
            if(page < 0 || page > oldest_page)
            {
                location.href.replace("404.html");
            }
            var start_number = Ref_len-1-(page-1)*APP;

            var data = snapshot.val();
            for(var i=start_number;i>=0 && i>start_number-APP;i--)
            {
                //alert(snapshot.val()[i].AID);

                    var article_link_str = "<a class='article_link' href='article.html?" + i + "'>"  + "</a>";
                    var writer_str = "<span id = 'writer'>" + data[i].name + "</span>";
                    var time_str = "<span id='time'>" + data[i].time + "</span>";
                    var evaluation_str = get_eval_str(data[i].evaluation);

                    total_ArticleTitle[total_ArticleTitle.length] = "<div class='each_block'><div class='each_article'>" + evaluation_str + " " + article_link_str + "</div><div>" + writer_str + " " + time_str + "</div></div>";
                    //alert(total_ArticleTitle[total_ArticleTitle.length]);
            }
            document.getElementById('Article_list').innerHTML = total_ArticleTitle.join('');
            var article_link = document.getElementsByClassName('article_link');
            for(var i=start_number;i>=0 && i>start_number-APP;i--)
            {
                article_link[start_number-i].textContent = '[' + data[i].category + ']' + data[i].title;
            }

            var turn_page = document.getElementById("turn_page");
            var oldest_page_Btn = "<a class='pageBtn' href='index.html?" + oldest_page + "'>" + "最舊" + "</a>";
            var last_page_Btn = (Number(page) != oldest_page) ? ("<a class='pageBtn' href='index.html?"+ (page+1) + "'>" + "‹ 上頁" + "</a>") : ("<a class='UselessBtn'>" + "‹ 上頁" + "</a>");
            //alert(oldest_page);
            var next_page_Btn = (Number(page) != 1) ? ("<a class='pageBtn' href = 'index.html?"+ (page-1) + "'>" + "下頁 ›" + "</a>") : ("<a class='UselessBtn'>" + "下頁 ›" + "</a>");
            var newest_page_Btn = "<a class='pageBtn' href='index.html?1'>" + "最新" + "</a>";
            turn_page.innerHTML = oldest_page_Btn + last_page_Btn + next_page_Btn + newest_page_Btn;
        })
        .catch(e => console.log(e.message));
        
        
}

window.onload = function()
{
    init();
}