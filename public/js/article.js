var Username = document.getElementById("Username");
var user_email;
var user_name;
var content = document.getElementById("content");
var title = document.getElementById("title");
var comment = document.getElementById("comment");
var text_comment = document.getElementById("text_comment");
var title_text = document.getElementsByClassName('title_text');
var author_text = document.getElementsByClassName('author_text');
var time_text = document.getElementsByClassName('time_text');
var content_text = document.getElementsByClassName('content_text');
var sendBtn = document.getElementById("sendBtn");
var a = location.href; 
var b = a.substring(a.indexOf("?")+1);
var article_ID = b;
var article_nickname;
var article_title;
var article_content;
var article_email;
var article_author;
var article_time;
var Ref = firebase.database().ref('article_list/' + article_ID);
var cmt_Ref = firebase.database().ref('article_list/' + article_ID + "/comment");

var each_cmt = document.getElementsByClassName('each_cmt');

function CurrentTime()
{
    //mins = ('0'+current_date.getMinutes()).slice(-2);
    var year = new Date().getFullYear();
    //var month = new Date().getMonth() + 1;
    var month = ('0' + (new Date().getMonth() + 1)).slice(-2);
    
    var day = ('0' + new Date().getDate()).slice(-2);
    
    var hour = ('0' + new Date().getHours()).slice(-2);
    
    var minute = ('0' + new Date().getMinutes()).slice(-2);
    
    return year + "/" + month + "/" + day + " " + hour + ":" + minute;
    //return year + "/" + month_zero + month + "/" + day_zero + day + " " + hour_zero + hour + ":" + minute_zero + minute;
}
var current_evaluation = 'like-Btn';
document.getElementsByClassName("like-Btn")[0].classList.add('steady');

function change_Btn(str)
{
    document.getElementsByClassName(current_evaluation)[0].classList.remove('steady');
    document.getElementsByClassName(str)[0].classList.add('steady');
    current_evaluation = str;
    //alert(current_evaluation);
}

function transfer_evaluation(str)
{
    if(str == "like-Btn")
    {
        return "<span id='like-cmt'>推 </span>";
    }
    else if(str == "hate-Btn")
    {
        return "<span id='hate-cmt'>噓 </span>";
    }
    else
    {
        return "<span id='norm-cmt'>→ </span>";
    }
}
function Cmt_Controller()
{
    var replace_str = text_comment.value.replace(/\r?\n/g, '');
    //alert(document.getElementById("exchange").textContent);
    var sent_str;
    var stay_str;
    if(replace_str.length > 30)
    {
        sent_str = replace_str.slice(0,30);
        text_comment.value = replace_str.slice(30);
    }
    else
    {
        sent_str = replace_str;
        text_comment.value = "";
    }

    if(replace_str != "")
    {
        cmt_Ref.once('value').then(function(snapshot) {
            //alert(snapshot.val()[0]);
            var cmt_len;
            if(snapshot.val() == null)   cmt_len = 0;
            else    cmt_len = snapshot.val().length;
            
            
            firebase.database().ref('article_list/' + article_ID + "/comment/" + cmt_len).set({
                cmt_content : sent_str,
                cmt_time : CurrentTime(),
                evaluation : current_evaluation,
                cmt_user :  user_name
            });
            
            Ref.once('value').then(function(snapshot) {
                var tmp = 0;
                if(current_evaluation == 'like-Btn')        tmp = 1;
                else if(current_evaluation == 'hate-Btn')   tmp -= 1;
                firebase.database().ref('article_list/' + article_ID + '/evaluation').set(snapshot.val().evaluation + tmp);
                //text_comment.value = stay_str;
                window.scrollTo(0,document.body.scrollHeight);
            });
            
            

            
            });
    }
}

function init()
{
    //alert("666");
    firebase.auth().onAuthStateChanged(function (user) {
        if(user)
        {
            user_email = user.email;
            user_name = user.displayName;
            Username.textContent = user_name;
        }
        else
        {
            window.location.href = "auth.html";
        }
    });


    
    
    
    Ref.once('value').then(function(snapshot) {
            if(snapshot.val() != null)
            {
                article_content = snapshot.val().content;
                article_title = snapshot.val().title;
                if(snapshot.val().nickname != "")
                {
                    article_nickname = "(" + snapshot.val().nickname + ")";
                }
                else    article_nickname = "";
                //alert(snapshot.val().nickname);
                article_email = snapshot.val().email;
                article_author = snapshot.val().name;
                article_time = snapshot.val().time;
                //title.innerHTML = "";
                var title_str1 = "<div><span id='title-name'>標題</span><span id='title-content' class='title_text'>" + "</span></div>";
                var title_str2 = "<div><span id='title-name'>作者</span><span id='title-content' class='author_text'>" + "</span></div>";
                var title_str3 = "<div><span id='title-name'>時間</span><span id='title-content' class='time_text'>" + "</span></div>";
                var some_words = "<span id='some-words'>※ 發信站: 友善論壇(friendly forum), 來自: 87.87.87.87</span><br>"
                            + "<span id='some-words'>※ 文章網址: " + location.href + "</span>";
                //
                
                content.innerHTML = "<div><span class='content_text'></span><br><br>--<br>" + some_words + "</div>";
                title.innerHTML = title_str1 + title_str2 + title_str3;
                title_text[0].textContent = article_title;
                author_text[0].textContent = article_author + article_nickname;
                time_text[0].textContent = article_time;
                content_text[0].innerText = article_content;
            }
            else
            {
                window.location.replace("404.html");
            }
      });
    Ref.once('value').then(function(snapshot){
                if(snapshot.val() != null)
                {
                    sendBtn.addEventListener('click',function(){

                        Cmt_Controller();
                    });


                    text_comment.addEventListener('keypress', function (e) {
                        var key = e.which || e.keyCode;

                        if (key === 13)
                        {   // 13 is enter
                            // code for enter
                            Cmt_Controller();
                        }
                    });
                }
    });
    
    //
    var first_count = 0;
    var second_count = 0;
    var total_comment = [];
    var total_cmt_content = [];
    cmt_Ref.once('value').then(function(snapshot) {

        snapshot.forEach(function(childshot){
            first_count += 1;
            var data = childshot.val();
            var evaluation_html = transfer_evaluation(data.evaluation);
            var space_str = "\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0";
            //alert('1'+ space_str +'1');
            var user_html ="<span id='cmt_User'>" + (data.cmt_user + space_str).slice(0, 11) + "</span>";
            var content_html = "<span class='each_cmt'></span>";
            total_comment[total_comment.length] = "<div>" + evaluation_html + user_html + ": " + content_html + " <span id='cmt_time'>" + data.cmt_time + "</span></div>";
            total_cmt_content[total_cmt_content.length] = data.cmt_content;
        });
        
        comment.innerHTML = total_comment.join("");
        for(var i=0;i<total_cmt_content.length;++i)
        {
            each_cmt[i].textContent = total_cmt_content[i];
        }
        
        cmt_Ref.on('child_added', function (childshot) {
            second_count += 1;
            if (second_count > first_count) {
                var data = childshot.val();
                var evaluation_html = transfer_evaluation(data.evaluation);
                var space_str = "\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0";
                var user_html ="<span id='cmt_User'>" + (data.cmt_user + space_str).slice(0, 11) + "</span>";
                var content_html = "<span class='each_cmt'></span>";
                total_comment[total_comment.length] = "<div>" + evaluation_html + user_html + ": " + content_html + " <span id='cmt_time'>" + data.cmt_time + "</span></div>";
                total_cmt_content[total_cmt_content.length] = data.cmt_content;
                comment.innerHTML = total_comment.join("");
                for(var i=0;i<total_cmt_content.length;++i)
                {
                    each_cmt[i].textContent = total_cmt_content[i];
                }
            }
        });

    });



    var like_Btn = document.getElementsByClassName("like-Btn");
    var hate_Btn = document.getElementsByClassName("hate-Btn");
    var norm_Btn = document.getElementsByClassName("norm-Btn");

    
    like_Btn[0].addEventListener('click',function(){
        if(current_evaluation != "like-Btn") change_Btn("like-Btn");
    });

    hate_Btn[0].addEventListener('click',function(){
        if(current_evaluation != "hate-Btn") change_Btn("hate-Btn");
    });

    norm_Btn[0].addEventListener('click',function(){
        if(current_evaluation != "norm-Btn") change_Btn("norm-Btn");
    });

    //alert(CurrentTime);

    


}


window.onload = function()
{
    init();
}